# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''


   joint: <label>, 
      in line, 
         <node 1>,
            <relative line position>,
            <relative orientation>,
         <node 2>,
            [ offset, <relative offset> ];#Still don't get this, jut used 0,0,0

<node 2> (or the point offset by <relative offset>) is constrained on the straight line fixed to <node 1> 
that goes through <relative line position> and is parallel to the z-direction defined by <relative orientation>. 
'''
import FreeCAD
import Draft

class Prismatic:
    def __init__(self, obj, label, node1, node2):
                
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z 
     
        obj.addExtension("App::GroupExtensionPython")  
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Prismatic joint","label",1).label = label
        obj.addProperty("App::PropertyString","joint","Prismatic joint","joint",1).joint = 'prismatic'
        obj.addProperty("App::PropertyString","node 1","Prismatic joint","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","Prismatic joint","node 2",1).node_2 = node2.label
        
        #Absolute position at the node 1 position, only for animation, not for MBDyn sumulation:  
        obj.addProperty("App::PropertyDistance","absolute pin position X","Absolute pin position","absolute pin position X",1).absolute_pin_position_X = x
        obj.addProperty("App::PropertyDistance","absolute pin position Y","Absolute pin position","absolute pin position Y",1).absolute_pin_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute pin position Z","Absolute pin position","absolute pin position Z",1).absolute_pin_position_Z = z

        obj.addProperty("App::PropertyString","animate","animation","animate").animate = 'false'

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']           

        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '2'
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0

        obj.Proxy = self
        
        #length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + node1.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p2 = FreeCAD.Vector(Llength, Llength, Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label          
        
    def execute(self, fp):
        #Get the node
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
            
        #Get the new absolute position:
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)
        
        #Update the absolute position:
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z                   
        
        FreeCAD.Console.PrintMessage("PRISMATIC JOINT: " +fp.label+" successful recomputation...\n")                    