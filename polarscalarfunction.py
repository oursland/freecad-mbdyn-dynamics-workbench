# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
#edge.discretize

import FreeCAD
import numpy as np

class PolarScalarFunction:
    def __init__(self, obj, label): 
        
        obj.addExtension("App::GroupExtensionPython")
                
        obj.addProperty("App::PropertyString","label","Scalar function","label",1).label = label
        obj.addProperty("App::PropertyString","function type","Scalar function","function type",1).function_type = "polar"
        obj.addProperty("App::PropertyString","node","Scalar function","node").node = "1"
        obj.addProperty("App::PropertyString","independent variable","Scalar function","independent variable").independent_variable = "Time"
        obj.addProperty("App::PropertyString","number of points per segment","Scalar function","number of points per segment").number_of_points_per_segment = ""
        obj.addProperty("App::PropertyInteger","resolution","Scalar function","resolution").resolution = 1
        obj.addProperty("App::PropertyEnumeration","invert","Scalar function","invert")
        obj.invert=['yes','no']
        #obj.addProperty("App::PropertyEnumeration","mode","Scalar function","mode")
        #obj.invert=['user-defined','proportional to the length of the edges']
        
        obj.addProperty("App::PropertyFloat","initial value","Domain","initial value").initial_value = FreeCAD.ActiveDocument.getObject("MBDyn").initial_time.Value
        obj.addProperty("App::PropertyFloat","final value","Domain","final value").final_value = FreeCAD.ActiveDocument.getObject("MBDyn").final_time.Value
        
        obj.addProperty("App::PropertyLinkSub","references","References","references")
        
        #function type
        obj.addProperty("App::PropertyEnumeration","type","Scalar function","type")
        obj.type=['cubicspline: cubic natural spline interpolation between the set of points (insert the points in the text file within this object)',
                  'multilinear: multilinear interpolation between the set of points (insert the points in the text file within this object)',
                  'chebychev: Chebychev interpolation between the set of points (insert the points in the text file within this object)']
  
        obj.addProperty("App::PropertyEnumeration","extrapolation","Scalar function","extrapolation")
        obj.extrapolation=['do not extrapolate', 'extrapolate']
                
        obj.Proxy = self

        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "points_"+ label)   
        obj.Label = 'points: '+ label
        
                               
        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: " + str(fp.node))[0]
        veccenter = FreeCAD.Vector(node.absolute_position_X, node.absolute_position_Y, node.absolute_position_Z)
        
        number_of_edges =  len(fp.references[1])
        
        edges = []
        
        for ed in range(number_of_edges):
            edges.append(fp.references[0].Shape.getElement(fp.references[1][ed]))
            
        #steps = ""
        
        #for line in edges:
        #    steps = steps + str(int(line.Length * fp.resolution)) + ","
            
        #steps = steps[:-1]
        
        #if fp.mode == "proportional to the length of the edges":        
        
        steps = fp.number_of_points_per_segment
        
        points = edges[0].discretize(Number = int(steps.split(",")[0])*fp.resolution)
        
        if fp.invert=="yes":
            points = points[::-1]
        
        lines = edges[1:]
        
        countpoints = 1

        for obj in lines:
            if len(steps.split(","))==1:
                aux = obj.discretize(Number = int(steps)*fp.resolution) 
            else:
                aux = obj.discretize(Number = int(steps.split(",")[countpoints])*fp.resolution) 
                countpoints = countpoints + 1
                
            if points[-1].sub(aux[0]).Length > points[-1].sub(aux[-1]).Length:
                aux = aux[::-1]
                    
            for obj1 in aux:
                points.append (obj1)

                                                                                                 
        Yvalues = []
        
        for n in points:
            Yvalues.append(n.sub(veccenter).Length/1000.)        
        
        obj = FreeCAD.ActiveDocument.getObjectsByLabel("points: " + fp.label)[0]
        obj.Text = ""
        
        InitialTime = FreeCAD.ActiveDocument.getObject("MBDyn").initial_time.Value
        FinalTime = FreeCAD.ActiveDocument.getObject("MBDyn").final_time.Value
        time = np.linspace(InitialTime, FinalTime, num = len(Yvalues)) 
        
        aux = 0
        for i in Yvalues:    
            obj.Text = obj.Text + str(time[aux])+ ',   '+str(i)+', \n'
            aux = aux + 1
        
        obj.Text = obj.Text[:-3]#Remove the last comma
        
        #Re-scale the function, in case the user has changed the range:
            
        obj = FreeCAD.ActiveDocument.getObjectsByLabel("points: "+fp.label)[0].Text.replace(" ","").replace("\n","").split(",")
        points = FreeCAD.ActiveDocument.getObjectsByLabel("points: "+fp.label)[0]
        Yvalues = obj[1::2]        
        steps = len(Yvalues)
        
        InitialValue = fp.initial_value
        FinalValue = fp.final_value
        
        time = np.linspace(InitialValue, FinalValue, num=steps) 
        
        
        points.Text = ""
        aux = 0
        for i in Yvalues:    
            points.Text = points.Text + '        ' + str(time[aux])+ ',   '+str(i)+', \n'
            aux = aux + 1    
        
        points.Text = points.Text[:-3]#Remove the last comma
        
        FreeCAD.Console.PrintMessage("SCALAR FUNCTION: " + fp.label + " successful recomputation...\n") 
        

            
            
                   
