# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import Draft

class Sphericalhinge:
    def __init__(self, obj, label, node1, node2):
                
        #Get the joint´s absolute position:
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
        
        #Calculate the relative offset:            
        x1 = x - node2.absolute_position_X
        y1 = y - node2.absolute_position_Y
        z1 = z - node2.absolute_position_Z

        obj.addExtension("App::GroupExtensionPython")          
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Spherical hinge","label",1).label = label        
        obj.addProperty("App::PropertyString","node 1","Spherical hinge","node 1",1).node_1 = node2.label
        obj.addProperty("App::PropertyString","node 2","Spherical hinge","node 2",1).node_2 = node1.label
        obj.addProperty("App::PropertyString","joint","Spherical hinge","joint",1).joint = 'spherical hinge'

        #Absolute position:
        obj.addProperty("App::PropertyDistance","absolute pin position X","Absolute pin position","absolute pin position X",1).absolute_pin_position_X = x
        obj.addProperty("App::PropertyDistance","absolute pin position Y","Absolute pin position","absolute pin position Y",1).absolute_pin_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute pin position Z","Absolute pin position","absolute pin position Z",1).absolute_pin_position_Z = z
        
        #Relative offset:          
        obj.addProperty("App::PropertyDistance","relative offset X","Relative offset","relative offset X",1).relative_offset_X = x1
        obj.addProperty("App::PropertyDistance","relative offset Y","Relative offset","relative offset Y",1).relative_offset_Y = y1
        obj.addProperty("App::PropertyDistance","relative offset Z","Relative offset","relative offset Z",1).relative_offset_Z = z1
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self
        
        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        #length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + node1.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p2 = FreeCAD.Vector(Llength, Llength, Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label                                                   
        
    def execute(self, fp):
       
        #Get the nodes
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        
        #Get the new absolute position:
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
        
        #Calculate the new relative offset:            
        x1 = node1.absolute_position_X - node2.absolute_position_X
        y1 = node1.absolute_position_Y - node2.absolute_position_Y
        z1 = node1.absolute_position_Z - node2.absolute_position_Z
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)
        
        #Update the absolute position:
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z
                        
        #Update the offset:
        fp.relative_offset_X = x1
        fp.relative_offset_Y = y1
        fp.relative_offset_Z = z1                          
        
        FreeCAD.Console.PrintMessage("SPHERICAL HINGE JOINT: " +fp.label+" successful recomputation...\n")               
               
