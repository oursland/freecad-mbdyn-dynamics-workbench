# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD

class Gear:                  
    def __init__(self, obj, BaseBody):#density in kg/mm^3 
        #the rigid body label is the same as the BaseBody label:
        label =  BaseBody.Label
        #The body is initially created with a density of 7900 kg/m^3, which is equivalent to 7.9e-06 kg/mm^3:
        density = FreeCAD.Units.Quantity(7.9e-06,FreeCAD.Units.Unit('kg/mm^3'))
        #obtain object's volume in m^3:      
        volume = FreeCAD.Units.Quantity(BaseBody.Shape.Volume,FreeCAD.Units.Unit('mm^3'))         
        #calculate object's mass, in kilograms:
        mass = FreeCAD.Units.Quantity(volume*density,FreeCAD.Units.Unit('kg'))
        #Returns moments of inertia divided by density:
        inertia = BaseBody.Shape.Solids[0].MatrixOfInertia
        
        #Store inertia moments without mass in mm^5:
        ii11 = FreeCAD.Units.Quantity(inertia.A[0],FreeCAD.Units.Unit('mm^5'))
        ii12 = FreeCAD.Units.Quantity(inertia.A[1],FreeCAD.Units.Unit('mm^5'))
        ii13 = FreeCAD.Units.Quantity(inertia.A[2],FreeCAD.Units.Unit('mm^5'))
        
        ii21 = FreeCAD.Units.Quantity(inertia.A[4],FreeCAD.Units.Unit('mm^5'))
        ii22 = FreeCAD.Units.Quantity(inertia.A[5],FreeCAD.Units.Unit('mm^5'))
        ii23 = FreeCAD.Units.Quantity(inertia.A[6],FreeCAD.Units.Unit('mm^5'))
        
        ii31 = FreeCAD.Units.Quantity(inertia.A[8],FreeCAD.Units.Unit('mm^5'))
        ii32 = FreeCAD.Units.Quantity(inertia.A[9],FreeCAD.Units.Unit('mm^5'))
        ii33 = FreeCAD.Units.Quantity(inertia.A[10],FreeCAD.Units.Unit('mm^5'))
        
        #Compute inertia moments with mass, in kg*mm^2:      
        i11 = FreeCAD.Units.Quantity(ii11*density,FreeCAD.Units.Unit('kg*mm^2'))
        i12 = FreeCAD.Units.Quantity(ii12*density,FreeCAD.Units.Unit('kg*mm^2'))
        i13 = FreeCAD.Units.Quantity(ii13*density,FreeCAD.Units.Unit('kg*mm^2'))
        
        i21 = FreeCAD.Units.Quantity(ii21*density,FreeCAD.Units.Unit('kg*mm^2'))
        i22 = FreeCAD.Units.Quantity(ii22*density,FreeCAD.Units.Unit('kg*mm^2'))
        i23 = FreeCAD.Units.Quantity(ii23*density,FreeCAD.Units.Unit('kg*mm^2'))        

        i31 = FreeCAD.Units.Quantity(ii31*density,FreeCAD.Units.Unit('kg*mm^2'))
        i32 = FreeCAD.Units.Quantity(ii32*density,FreeCAD.Units.Unit('kg*mm^2'))
        i33 = FreeCAD.Units.Quantity(ii33*density,FreeCAD.Units.Unit('kg*mm^2')) 
        
        #Compute absolute center of mass, relative to global frame, in mm:
        cmx = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        cmy = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        cmz = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
        #since initially the node will be placed at the center of mass, the relative center of mass is [0,0,0]mm:
        cmxx = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        cmyy = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        cmzz = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        
        #spring:
        st = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('N*m/rad'))
        
        #Give the object the capability to store other objects:
        obj.addExtension("App::GroupExtensionPython") 
        
        #Rigid body identifiers:       
        obj.addProperty("App::PropertyString","label","Rigid body","A unique numerical label that identifies this body is automatically assigned",1).label = label
        obj.addProperty("App::PropertyString","type","Rigid body","The type of the body is: rigid",1).type = 'rigid: gear'
        obj.addProperty("App::PropertyString","node","Rigid body","The label of the node associated to this body",1).node = label #The body's asociated structural node  
        
        #Gear properties:
        obj.addProperty("App::PropertyInteger","teeth","User-defined gear properties",'Number of teeth []. To access this variable type "teeth_" followed by the number of gear, for instance "teeth_1".').teeth = 0
        obj.addProperty("App::PropertyDistance","radius","User-defined gear properties",'Radius. To access this variable type "radius_" followed by the number of gear, for instance "radius_1".').radius =  FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","module","User-defined gear properties",'Module. To access this variable type "module_" followed by the number of gear, for instance "module_1".').module = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyQuantity","k_ground_spring","User-defined gear properties",'Stiffnes coefficient of ground spring. To access this variable type "k_ground_spring_" followed by the number of gear, for instance "k_ground_spring_1".').k_ground_spring = st.Value 
        obj.k_ground_spring = FreeCAD.Units.Unit('N*m/rad')
    
        #Rigid body physical properties:         
        obj.addProperty("App::PropertyQuantity","density","Physical properties",'density',1).density = density.Value 
        obj.density = FreeCAD.Units.Unit('kg/mm^3')
        
        obj.addProperty("App::PropertyQuantity","volume","Physical properties",'To access this variable type "volume_" followed by the number of body, for instance "volume_1".',1).volume = volume.Value        
        obj.volume = FreeCAD.Units.Unit('mm^3')
        
        obj.addProperty("App::PropertyQuantity","mass","Physical properties",'To access this variable type "mass_" followed by the number of body, for instance "mass_1".',1).mass = mass.Value 
        obj.mass = FreeCAD.Units.Unit('kg')
        
        obj.addProperty("App::PropertyString","material","Physical properties","material",1).material = 'Steel-Generic'#Material´s name is generic steel
        
        #absolute center of mass is the center of mass relative to the absolute coordinate system:
        obj.addProperty("App::PropertyDistance","absolute_center_of_mass_X","Absolute center of mass","X component of the absolute center of mass",1).absolute_center_of_mass_X = cmx
        obj.addProperty("App::PropertyDistance","absolute_center_of_mass_Y","Absolute center of mass","Y component of the absolute center of mass",1).absolute_center_of_mass_Y = cmy
        obj.addProperty("App::PropertyDistance","absolute_center_of_mass_Z","Absolute center of mass","Z component of the absolute center of mass",1).absolute_center_of_mass_Z = cmz
        
        #since initially the node is at the center of mass, the relative center of mass (relative to the node) is [0,0,0]:
        obj.addProperty("App::PropertyDistance","relative_center_of_mass_X","Center of mass relative to node","X component of the center of mass relative to the node",1).relative_center_of_mass_X = cmxx     
        obj.addProperty("App::PropertyDistance","relative_center_of_mass_Y","Center of mass relative to node","Y component of the center of mass relative to the node",1).relative_center_of_mass_Y = cmyy
        obj.addProperty("App::PropertyDistance","relative_center_of_mass_Z","Center of mass relative to node","Z component of the center of mass relative to the node",1).relative_center_of_mass_Z = cmzz     
        
        #Moments of inertia with mass:
        obj.addProperty("App::PropertyQuantity","Ixx","Moments of inertia with mass",'To access this variable type "Ixx_" followed by the number of body, for instance "Ixx_1".',1).Ixx = i11.Value
        obj.Ixx = FreeCAD.Units.Unit('kg*mm^2')        
        obj.addProperty("App::PropertyQuantity","Ixy","Moments of inertia with mass",'To access this variable type "Ixy_" followed by the number of body, for instance "Ixy_1".',1).Ixy = i12.Value
        obj.Ixy = FreeCAD.Units.Unit('kg*mm^2')
        obj.addProperty("App::PropertyQuantity","Ixz","Moments of inertia with mass",'To access this variable type "Ixz_" followed by the number of body, for instance "Ixz_1".',1).Ixz = i13.Value
        obj.Ixz = FreeCAD.Units.Unit('kg*mm^2')

        obj.addProperty("App::PropertyQuantity","Iyx","Moments of inertia with mass",'To access this variable type "Iyx_" followed by the number of body, for instance "Iyx_1".',1).Iyx = i21.Value
        obj.Iyx = FreeCAD.Units.Unit('kg*mm^2')
        obj.addProperty("App::PropertyQuantity","Iyy","Moments of inertia with mass",'To access this variable type "Iyy_" followed by the number of body, for instance "Iyy_1".',1).Iyy = i22.Value
        obj.Iyy = FreeCAD.Units.Unit('kg*mm^2')
        obj.addProperty("App::PropertyQuantity","Iyz","Moments of inertia with mass",'To access this variable type "Iyz_" followed by the number of body, for instance "Iyz_1".',1).Iyz = i23.Value
        obj.Iyz = FreeCAD.Units.Unit('kg*mm^2')
        
        obj.addProperty("App::PropertyQuantity","Izx","Moments of inertia with mass",'To access this variable type "Izx_" followed by the number of body, for instance "Izx_1".',1).Izx = i31.Value
        obj.Izx = FreeCAD.Units.Unit('kg*mm^2')
        obj.addProperty("App::PropertyQuantity","Izy","Moments of inertia with mass",'To access this variable type "Izy_" followed by the number of body, for instance "Izy_1".',1).Izy = i32.Value
        obj.Izy = FreeCAD.Units.Unit('kg*mm^2')
        obj.addProperty("App::PropertyQuantity","Izz","Moments of inertia with mass",'To access this variable type "Izz_" followed by the number of body, for instance "Izz_1".',1).Izz = i33.Value
        obj.Izz = FreeCAD.Units.Unit('kg*mm^2')
        
        #Moments of inertia without mass:
        obj.addProperty("App::PropertyQuantity","ixx","Moments of inertia without mass (divided by density)","ii11",1).ixx = ii11.Value
        obj.ixx = FreeCAD.Units.Unit('mm^5')
        obj.addProperty("App::PropertyQuantity","ixy","Moments of inertia without mass (divided by density)","ii12",1).ixy = ii12.Value
        obj.ixy = FreeCAD.Units.Unit('mm^5')
        obj.addProperty("App::PropertyQuantity","ixz","Moments of inertia without mass (divided by density)","ii13",1).ixz = ii13.Value
        obj.ixz = FreeCAD.Units.Unit('mm^5')
        
        obj.addProperty("App::PropertyQuantity","iyx","Moments of inertia without mass (divided by density)","ii21",1).iyx = ii21.Value
        obj.iyx = FreeCAD.Units.Unit('mm^5')
        obj.addProperty("App::PropertyQuantity","iyy","Moments of inertia without mass (divided by density)","ii22",1).iyy = ii22.Value
        obj.iyy = FreeCAD.Units.Unit('mm^5')
        obj.addProperty("App::PropertyQuantity","iyz","Moments of inertia without mass (divided by density)","ii23",1).iyz = ii23.Value
        obj.iyz = FreeCAD.Units.Unit('mm^5')

        obj.addProperty("App::PropertyQuantity","izx","Moments of inertia without mass (divided by density)","ii31",1).izx = ii31.Value
        obj.izx = FreeCAD.Units.Unit('mm^5')
        obj.addProperty("App::PropertyQuantity","izy","Moments of inertia without mass (divided by density)","ii32",1).izy = ii32.Value
        obj.izy = FreeCAD.Units.Unit('mm^5')
        obj.addProperty("App::PropertyQuantity","izz","Moments of inertia without mass (divided by density)","ii33",1).izz = ii33.Value
        obj.izz = FreeCAD.Units.Unit('mm^5')
        

        obj.Proxy = self#needed to execute on_changed methods
                
    def execute(self, fp):        
        #The label is used to obtain other objects related to this body:
        label = fp.label
        #Get the shape from the corresponding parametric body, to calculate the inertia:
        basebody = FreeCAD.ActiveDocument.getObjectsByLabel(label)[0]
        fp.Shape = basebody.Shape                             
        #The inertia matrix is the same of the object, because the object´s shape is inherited from the original body. Get the inertia matrix:
        inertia = basebody.Shape.Solids[0].MatrixOfInertia
        #get the new density (defined by the user in the Material object):
        densityaux = FreeCAD.ActiveDocument.getObjectsByLabel("material: "+fp.label)[0].Material['Density']#returns a string such as '7900.00 kg/m^3'
        density = FreeCAD.Units.Quantity(float(densityaux.split(' ')[0])/1000.0**3,FreeCAD.Units.Unit('kg/mm^3'))#Convert density to the appropriate units, to calculate moments of inertia
        #get the new material (defined by the user in the Material object):
        material = FreeCAD.ActiveDocument.getObjectsByLabel("material: "+fp.label)[0].Material['Name']#Returns a string        
        #get the new volume:
        volume = FreeCAD.Units.Quantity(fp.Shape.Volume,FreeCAD.Units.Unit('mm^3'))  
        #calculate the new object's mass, in kilograms:
        mass = FreeCAD.Units.Quantity(volume*density,FreeCAD.Units.Unit('kg'))
        
        #get the new inertia moments without mass:
        ii11 = FreeCAD.Units.Quantity(inertia.A[0],FreeCAD.Units.Unit('mm^5'))
        ii12 = FreeCAD.Units.Quantity(inertia.A[1],FreeCAD.Units.Unit('mm^5'))
        ii13 = FreeCAD.Units.Quantity(inertia.A[2],FreeCAD.Units.Unit('mm^5'))

        ii21 = FreeCAD.Units.Quantity(inertia.A[4],FreeCAD.Units.Unit('mm^5'))
        ii22 = FreeCAD.Units.Quantity(inertia.A[5],FreeCAD.Units.Unit('mm^5'))
        ii23 = FreeCAD.Units.Quantity(inertia.A[6],FreeCAD.Units.Unit('mm^5'))

        ii31 = FreeCAD.Units.Quantity(inertia.A[8],FreeCAD.Units.Unit('mm^5'))
        ii32 = FreeCAD.Units.Quantity(inertia.A[9],FreeCAD.Units.Unit('mm^5'))
        ii33 = FreeCAD.Units.Quantity(inertia.A[10],FreeCAD.Units.Unit('mm^5'))

        #compute new inertia moments, in kg*mm^2: 
        i11 = FreeCAD.Units.Quantity(ii11*density,FreeCAD.Units.Unit('kg*mm^2'))
        i12 = FreeCAD.Units.Quantity(ii12*density,FreeCAD.Units.Unit('kg*mm^2'))
        i13 = FreeCAD.Units.Quantity(ii13*density,FreeCAD.Units.Unit('kg*mm^2'))

        i21 = FreeCAD.Units.Quantity(ii21*density,FreeCAD.Units.Unit('kg*mm^2'))
        i22 = FreeCAD.Units.Quantity(ii22*density,FreeCAD.Units.Unit('kg*mm^2'))
        i23 = FreeCAD.Units.Quantity(ii23*density,FreeCAD.Units.Unit('kg*mm^2'))

        i31 = FreeCAD.Units.Quantity(ii31*density,FreeCAD.Units.Unit('kg*mm^2'))
        i32 = FreeCAD.Units.Quantity(ii32*density,FreeCAD.Units.Unit('kg*mm^2'))
        i33 = FreeCAD.Units.Quantity(ii33*density,FreeCAD.Units.Unit('kg*mm^2'))
        
        #Compute the new absolute center of mass, relative to global frame:
        cmx = FreeCAD.Units.Quantity(fp.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        cmy = FreeCAD.Units.Quantity(fp.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        cmz = FreeCAD.Units.Quantity(fp.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))        

        #UPDATE THE VARIABLES:

        #Update mass
        fp.mass = str(mass.Value)+' kg' 

        #Update density
        fp.density = str(density.Value)+' kg/mm^3'

        #Update material
        fp.material = material
        
        #Update volume
        fp.volume = str(volume.Value)+' mm^3'
        
        #Update moments of inertia with mass:
        fp.Ixx = i11.Value
        fp.Ixy = i12.Value
        fp.Ixz = i13.Value

        fp.Iyx = i21.Value
        fp.Iyy = i22.Value
        fp.Iyz = i23.Value

        fp.Izx = i31.Value
        fp.Izy = i32.Value
        fp.Izz = i33.Value
        
        #Update moments of inertia witout mass:
        fp.ixx = ii11.Value
        fp.ixy = ii12.Value
        fp.ixz = ii13.Value

        fp.iyx = ii21.Value
        fp.iyy = ii22.Value
        fp.iyz = ii23.Value  

        fp.izx = ii31.Value
        fp.izy = ii32.Value
        fp.izz = ii33.Value
         
        #Update the absolute center of mass:
        fp.absolute_center_of_mass_X = cmx
        fp.absolute_center_of_mass_Y = cmy
        fp.absolute_center_of_mass_Z = cmz
        
        #recalculate the new relative center of mass, in case the node has been moved:          
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label))==1):#Only if the node has already been created
            #get the corresponding node's absolute possition:
            xcc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].absolute_position_X
            ycc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].absolute_position_Y
            zcc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].absolute_position_Z
            #Update the body's relative center of mass position:
            fp.relative_center_of_mass_X = fp.absolute_center_of_mass_X - xcc
            fp.relative_center_of_mass_Y = fp.absolute_center_of_mass_Y - ycc
            fp.relative_center_of_mass_Z = fp.absolute_center_of_mass_Z - zcc
            
            FreeCAD.Console.PrintMessage("RIGID BODY: " +fp.label+ " successful recomputation...\n")
            
        else:#If there is no structural node asociated to the body, issue an error:
        
            FreeCAD.Console.PrintMessage("RIGID BODY: " +fp.label+ ': Warning, no structural node asociated to this body. Relative center of mass cannot be calculated.\n')
 
    def onDocumentRestored(self, fp):
        fp.Ixx = FreeCAD.Units.Unit('kg*mm^2')        
        fp.Ixy = FreeCAD.Units.Unit('kg*mm^2')        
        fp.Ixz = FreeCAD.Units.Unit('kg*mm^2')        
        
        fp.Iyx = FreeCAD.Units.Unit('kg*mm^2')        
        fp.Iyy = FreeCAD.Units.Unit('kg*mm^2')        
        fp.Iyz = FreeCAD.Units.Unit('kg*mm^2')        
        
        fp.Izx = FreeCAD.Units.Unit('kg*mm^2')        
        fp.Izy = FreeCAD.Units.Unit('kg*mm^2')        
        fp.Izz = FreeCAD.Units.Unit('kg*mm^2')        
        
        fp.ixx = FreeCAD.Units.Unit('mm^5')    
        fp.ixy = FreeCAD.Units.Unit('mm^5')  
        fp.ixy = FreeCAD.Units.Unit('mm^5')  
        
        fp.iyx = FreeCAD.Units.Unit('mm^5')    
        fp.iyy = FreeCAD.Units.Unit('mm^5')  
        fp.iyz = FreeCAD.Units.Unit('mm^5') 
        
        fp.izx = FreeCAD.Units.Unit('mm^5')    
        fp.izy = FreeCAD.Units.Unit('mm^5')  
        fp.izz = FreeCAD.Units.Unit('mm^5')  
        
        fp.density = FreeCAD.Units.Unit('kg/mm^3')
        fp.mass = FreeCAD.Units.Unit('kg')
        fp.volume = FreeCAD.Units.Unit('mm^3')
        
        fp.k_ground_spring = FreeCAD.Units.Unit('N*m/rad')

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        
     

